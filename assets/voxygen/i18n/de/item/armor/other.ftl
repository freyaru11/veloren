glider-basic_red = Roter Stoffgleiter
    .desc = Ein einfacher Gleiter, mit auffällig roter Farbe.
lantern-magic_lantern = Magische Laterne
    .desc = Erleuchtet sogar das dunkelste Verlies. Ein großes Monster wurde dafür erlegt.
lantern-black = Schwarze Laterne
    .desc = Recht gewöhnlich durch beliebte Nutzung angehender Abenteurer!
lantern-blue = Kalte blaue Laterne
    .desc = Entfacht ist diese Laterne überraschend kalt.
