common-abilities-debug-possess = Pfeil der Besessenheit
    .desc = Schießt einen giftigen Pfeil und lässt dich dein Ziel kontrollieren.
common-abilities-hammer-leap = Schicksalsschlag
    .desc = Eine Attacke mit Flächeneffekt und Rückstoß. Spring an die Position des Cursors.
common-abilities-bow-shotgun = Pfeilhagel
    .desc = Schießt einen Pfeilhagel ab
common-abilities-staff-fireshockwave = Ring des Feuers
    .desc = Entzündet einen Feuerring um dich herum.
common-abilities-sceptre-wardingaura = Abwehrende Aura
    .desc = Schützt deine Verbündeten vor feindlichen Angriffen.
common-abilities-sword-crippling_gouge = Verstümmelnder Hieb
    .desc = Fügt deinem Gegner eine bleibende Wunde zu.
common-abilities-sword-heavy_fortitude = Schwerer Standhaftigkeit
    .desc = Du stabilisierst dich, so dass die nächsten Schläge dich nicht ins Wanken bringen.
common-abilities-sword-agile_dual_perforate = Durchlöchern
    .desc = Ein schneller Wirbelwind kleiner Angriffe mit beiden Schwertern
common-abilities-sword-agile_perforate = Durchlöchern
    .desc = Ein schneller Wirbelwind kleiner Angriffe
common-abilities-sword-heavy_double_slash = Schwerer Doppelschlag
    .desc = Eine langsame Doppelschlag-Combo, die ins Taumeln bringen kann
common-abilities-sword-cleaving_dual_spiral_slash = Rundumschlag
    .desc = Schwing deine beiden Klingen um dich, um jeden in der Nähe zu treffen
common-abilities-sword-cleaving_spiral_slash = Rundumschlag
    .desc = Schwing deine Klinge um dich, um jeden in der Nähe zu treffen
common-abilities-sword-crippling_deep_rend = Tiefes Zerfleischen
    .desc = Ein Schlag, der auf eine bereits offene Wunde zielt, fügt blutenden Gegnern mehr Schaden zu
